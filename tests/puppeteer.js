'use strict';

const puppeteer = require('puppeteer');

// This is a simple test of the generated image to ensure
// puppeteer can launch and open a page.
(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://github.com/puppeteer/puppeteer');
  await page.screenshot({ path: 'puppeteer.png' });
  await browser.close();
})();
