# Changelog

## v10.0.0 (2023-07-02)

### Changed

- BREAKING: The Node 14 and 19 container images were removed from this
  project's container registry. (#50, #51)
- BREAKING: Updated Node 16 images to Debian Bullseye. (#55)

### Miscellaneous

- Excluded unnecessary jobs from tag pipelines, which only creates the
  release. (#54)

## v9.3.0 (2023-06-04)

### Changed

- Deprecate Node 19 since end-of-life as of 2023-06-01. Images are still
  generated for all current and LTS releases (Node `16`, `18`, `20`). (#52)
  - **Node 19 images are still available, but will be removed from this
    project's container registry on 2023-06-30.**

## v9.2.0 (2023-04-30)

### Changed

- Deprecate Node 14 since end-of-life as of 2023-04-30. Images are still
  generated for all current and LTS releases (Node `16`, `18`,`19`, `20`). (#49)
  - **Node 14 images are still available, but will be removed from this
    project's container registry on 2023-06-30.**

## v9.1.0 (2023-04-20)

### Changed

- Added container image for Node 20. (#48)

### Miscellaneous

- Updated container image test names for consistency. (#47)

## v9.0.1 (2022-11-02)

### Fixed

- Added `procps` package to images, which was removed when moving to Debian Slim, to resolve issues with spawning child processes in some cases. (#46)

## v9.0.0 (2022-10-30)

### Changed

- BREAKING: Updated base image from the applicable Node Debian image to the Node Debian Slim image, which significantly reduces image size and eliminates a large number of vulnerabilities. This is now reflected in the versioned image names, e.g. `node-18.12.0-bullseye-slim`. The Node major version tags (e.g. `node-18`) and `latest` tags have also been updated, but the names remain unchanged. Details on the rationale and differences in the images can be found in the [documentation](docs/debain-vs-debian-slim.md). (#44)
  - If needed, the latest image tags built with the full Debian images are `node-14.20.1-buster`, `node-16.18.0-buster`, `node-18.12.0-bullseye`, `node-19.0.0-bullseye`. All older Debian images will remain until removed per the [standard retention policy](/README.md#docker-puppeteer-images)
- BREAKING: Updated `latest` image tag to the latest Node 18 image (the Active LTS release as of 2022-10-25). (#45)
- Added container images for Node 19 based on Debian Bullseye. (#43)
- Added standard set of `LABEL`s to images (documentation, license, etc). (#42)

## v8.0.0 (2022-08-31)

### Changes

- BREAKING: Updated OS packages installed in all container images to establish a common set compatibile with Debian Bullseye (the default for Node 18) and previous releases. All images are now built with this common configuration, which has been tested with Puppeteer v9, v15, v16, and v17. Note this is a different configuration than the new Puppeteer "official" Dockerfile, with details [captured here](README.md#differences-from-the-puppeteer-dockerfile) (#28)
- BREAKING: Updated all `node-18` images (starting with 18.8.0) to Debian Bullseye. The previously built images remain as-is (with the latest Debian Buster at build time) (#28)
- BREAKING: Updated all `node-14` images (starting with 14.20.0) to Debian Buster since Debian Stretch is end-of-life. The previously built images remain as-is (with the latest Debian Stretch at build time) (#40)

### Miscellaneous

- Updated CI pipeline for container images with a dedicate `pa11y` test with `puppeteer` v9.1.1 (in addition to a test with the latest `puppeteer)

## v7.1.0 (2022-07-15)

### Changed

- Updated image build strategy to continue to keep one tag for the latest of each Node major releases (e.g. `node-16`), but also images for each specific Node release (e.g. `16.16.0-buster`) to simplify pinning or rollback. (#39)

### Miscellaneous

- Updated to latest group `renovate` docker config which only pins image digests if a versioned tag is used. (#36)

## v7.0.0 (2022-06-02)

### Changed

- BREAKING: Removed Node 17 image since end-of-life. Images generated for all current and LTS releases (latest Node `14`, `16`, `18`). (#33)
- Removed Node 12 images from container registry (deprecated in v6.0.0 since Node 12 is end-of-life). (#34)

## v6.0.0 (2022-05-02)

### Changed

- BREAKING: Removed Node 12 image since end-of-life. Images generated for all current and LTS releases (latest Node `14`, `16`, `17`, `18`). (#32)
- Added container image for Node 18, uses Debian Buster due to deprecated library in Debian Bullseye, which is the default for Node 18 (#31).

## v5.0.1 (2021-11-03)

### Fixed

- Add current OS version to image tags in `.gitlab-ci.yml` to identify them explicitly (#24)

### Miscellaneous

- Update renovate config to remove `gitlabci-include` override, which is now part of renovate (as of `28.15.0`) (#30)

## v5.0.0 (2021-10-27)

### Changed

- BREAKING: Updated `latest` image to Node 16, which is now the active LTS release. (#27)

### Miscellaneous

- Updated `renovate` config to include tracking for all Docker template `include`s (#29)

## v4.2.0 (2021-10-20)

### Added

- Added container image for Node 17, uses Debian Buster due to deprecated library in Debian Bullseye, which is the default for Node 17 (#26, #28).

### Changed

- Augmented container test with screenshot to verify proper page load (#25)

### Miscellaneous

- Add [`gitlab-releaser`](https://gitlab.com/gitlab-ci-utils/gitlab-releaser) file to set milestones for releases.

## v4.1.0 (2021-10-10)

### Changed

- Moved to container build/test/deploy pipeline leveraging `kaniko` for build and `skopeo` for deploy. (#20)

### Miscellaneous

- Setup [renovate](https://docs.renovatebot.com/) for dependency updates, including configuring tracking Node versions in CI pipeline (#19)
- Add simple puppeteer test with image before deploy (#18)

## v4.0.0 (2021-06-02)

### Changed

- BREAKING: Deprecated Node 15 support since end-of-life. Compatible with all current and LTS releases (`^12.20.0 || ^14.15.0 || >=16.0.0`). (#14)

## v3.0.0 (2021-05-01)

### Changed

- BREAKING: Deprecate Node 10 support since end-of-life (#15)

## v2.2.0 (2021-04-27)

### Added

- Added container image for Node v16 (#13)

### Changed

- Updated Dockerfile to add libgbm1 to resolve Chrome launching issue in Node v16 (#13)

### Miscellaneous

- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#9), use matrix syntax for Node versions (#11), and clean up `latest` tag for Node LTS release (#12)

## v2.1.0 (2020-10-27)

### Changed

- Updated Node LTS from v12 to v14. (#5)

## v2.0.0 (2020-10-23)

### Changed

- BREAKING: Remove version numbered images. (#8)

## v1.2.0 (2020-10-23)

### Added

- Add image for Node 15 (#6)

### Changed

- Update pipeline to build all images by leveraging a common template, using ARG in Dockerfile to specify version, and spawning child pipelines for each image. (#7)

## v1.1.0 (2020-06-03)

### Changed

- Removed Node 13 references and scheduled build since end-of-life as of 2020-06-01 (#3)

## v1.0.0 (2019-09-28)

### Initial Release

- Initial container image with Puppeteer's recommended configuration.
